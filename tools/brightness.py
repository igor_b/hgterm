#!/usr/bin/env python

import pigpio
import sys

PIN = 19
VALS = (0, 10, 30, 50, 70, 100, 150, 255)

if __name__=="__main__":
    pi = pigpio.pi()
    try:
        curr = pi.get_PWM_dutycycle(PIN)
    except pigpio.error:
        curr = VALS[-1]
        pi.set_mode(PIN, pigpio.ALT5)
        pi.set_PWM_frequency(PIN, 100)
        pi.set_PWM_dutycycle(PIN, curr)
    try:
        idx = VALS.index(curr)
    except ValueError:
        idx = len(VALS)-1

    try:
        arg = sys.argv[1]
    except IndexError:
        print("Current brightness %d%%"%(int(float(idx)/(len(VALS)-1)*100),))
        sys.exit(0)

    if arg not in ('+', '-',) or arg in ('-h', '--help',):
        print("Usage:")
        print("  %s [+|-]"%(sys.argv[0],))
        sys.exit(1)

    idx = idx+1 if arg=='+' else idx-1
    idx = max(0, min(len(VALS)-1, idx))
    pi.set_PWM_dutycycle(PIN, VALS[idx])

