// Based on the code from http://raspberrypi.stackexchange.com/a/92797/
// compile with:
// gcc -Wall `pkg-config --cflags gtk+-2.0 lxpanel` -shared -fPIC hgbatstat.c -o hgbatstat.so `pkg-config --libs lxpanel`
// To install:
// sudo cp hgbatstat.so /usr/lib/arm-linux-gnueabihf/lxpanel/plugins/hgbatstat.so

#include <lxpanel/plugin.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>

#define LabelSize 32

float get_battery_voltage(){
  const int addr = 0x44;
  uint8_t reg = 0x02;
  uint8_t val[2];
  int file;
  char *fn = "/dev/i2c-3";
  if((file=open(fn, O_RDWR))<0){
    perror("Failed to open i2c bus");
    return -1;
  }
  if(ioctl(file, I2C_SLAVE, addr)<0){
    printf("failed to acquire bus access\n");
    return -1;
  }
  if(write(file, &reg, 1)!=1){
    printf("failed write\n");
    return -1;
  }
  if(read(file, &val, 2)!=2){
    printf("failed read\n");
    return -1;
  }
  return (float)((int)val[1]<<8|val[0])/1000.0;
}

void update_label(GtkLabel *l){
  float v = get_battery_voltage();
  char t[20];
  if(v==-1){
    snprintf(t, 20, " B:N/A ");
  }
  else{
    snprintf(t, 20, " B:%.2fV ", v);
  }
  gtk_label_set_text(l, t);
}


gint timer_callback(gpointer data){
  update_label((GtkLabel*)data);
  return 1;
}

GtkWidget *batstat_constructor(LXPanel *panel, config_setting_t *settings){
 /* panel is a pointer to the panel and
     settings is a pointer to the configuration data
     since we don't use it, we'll make sure it doesn't
     give us an error at compilation time */
 (void)panel;
 (void)settings;

 // create a label widget instance (should add some alignment/padding)
 GtkWidget *pLabel = gtk_label_new("");
 update_label((GtkLabel*)pLabel);

 // set the label to be visible
 gtk_widget_show(pLabel);

 // need to create a container to be able to set a border
 GtkWidget *p = gtk_event_box_new();

 // update label every 8s
 gtk_timeout_add (8000, timer_callback, (gpointer)pLabel);

 // our widget doesn't have a window...
 // it is usually illegal to call gtk_widget_set_has_window() from application but for GtkEventBox it doesn't hurt
 gtk_widget_set_has_window(p, FALSE);

 // set border width
 gtk_container_set_border_width(GTK_CONTAINER(p), 2);

 // add the label to the container
 gtk_container_add(GTK_CONTAINER(p), pLabel);

 // set the size we want
 // gtk_widget_set_size_request(p, 100, 25);

 // success!!!
 return p;
}

FM_DEFINE_MODULE(lxpanel_gtk, batstat)

/* Plugin descriptor. */
LXPanelPluginInit fm_module_init_lxpanel_gtk = {
   .name = "hgTerm Battery Status",
   .description = "Display battery voltage and percentage",
   .one_per_system = 1,
   .new_instance = batstat_constructor
};
