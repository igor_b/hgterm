#include "Arduino.h"
#include <Wire.h>

//
// 
// REG         NAME               READ          WRITE
// 0x00     ID                    0x68 0x67     --
// 0x01     heartbeat             --            anything
// 0x02     battery status        V*1000        --
// 0x03     button status         0x00|0x01     --
//
// 0x10     D0                    0x00|0x01     0x00|0x01
// 0x11     D0 mode               mode          INPUT/OUTPUT/INPUT_PU
// 0x20     D1                    0x00|0x01     0x00|0x01
// 0x21     D1 mode               mode          INPUT/OUTPUT/INPUT_PU
// 0x30     D2                    0x00|0x01     0x00|0x01
// 0x31     D2 mode               mode          INPUT/OUTPUT/INPUT_PU
// 0x40     D3                    0x00|0x01     0x00|0x01|0-255
// 0x41     D3 mode               mode          INPUT/OUTPUT/INPUT_PU/PWM
// 0x50     D5                    0x00|0x01     0x00|0x01|0-255
// 0x51     D5 mode               mode          INPUT/OUTPUT/INPUT_PU/PWM
// 0x60     A3                    0x00|0x01     0x00|0x01
// 0x61     A3 mode               mode          INPUT/OUTPUT/INPUT_PU/ANALOG
// 0x62     A3 analog             V*1000        --
// 0x70     A2                    0x00|0x01     0x00|0x01
// 0x71     A2 mode               mode          INPUT/OUTPUT/INPUT_PU/ANALOG
// 0x72     A2 analog             V*1000        --
// 0x80     A1                    0x00|0x01     0x00|0x01
// 0x81     A1 mode               mode          INPUT/OUTPUT/INPUT_PU/ANALOG
// 0x82     A1 analog             V*1000        --
//  
//  Pin modes:
//    0x00  INPUT
//    0x01  OUTPUT
//    0x02  INPUT_PULLUP
//    0x03  PWM
//    0x04  ANALOG

enum pin_mode_t{
  MODE_INPUT,
  MODE_OUTPUT,
  MODE_INPUT_PULLUP,
  MODE_PWM,
  MODE_ANALOG,
  MODE_END
};

const int battery_pin = A0;
const int button_pin = 9;
const int power_pin = 8;
const int button_poweroff_timeout = 5000;

const uint8_t pin_map[] = {0, 1, 2, 3, 5, A3, A2, A1};
uint8_t pin_modes[] = {
  MODE_INPUT, MODE_INPUT, MODE_INPUT, MODE_INPUT, 
  MODE_INPUT, MODE_ANALOG, MODE_ANALOG, MODE_ANALOG, 
};

const uint8_t I2C_ADDRESS = 0x44;
uint32_t last_activity = 0;

int proc_cmd = -1;


uint16_t readVcc() {
	// https://hackingmajenkoblog.wordpress.com/2016/02/01/making-accurate-adc-readings-on-the-arduino/
	uint16_t result;
	// Read 1.1V reference against AVcc
	ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
	delay(2); // Wait for Vref to settle
	ADCSRA |= _BV(ADSC); // Convert
	while (bit_is_set(ADCSRA,ADSC));
	result = ADCL;
	result |= ADCH<<8;
	//result = 1125300L / result; // Back-calculate AVcc in mV (datasheet value)
  // conversion constant is the voltage of internal voltage reference in mV multiplied by 1023
  // internal voltage reference can be anything between 1.1-1.2V
  // in this certain case measured voltage reference is 1.1879V
	result = 1215300L / result; // Back-calculate AVcc in mV (measured value)
	return result;
}


uint16_t betterAnalogRead(const int pin){
  uint16_t vcc = readVcc();
  uint16_t val = analogRead(pin);
  return (uint16_t)((uint32_t)val*(uint32_t)vcc/(uint32_t)1024);
}


void setPinMode(int pin, int mode){
  if(pin<0 || pin>7) return;
  switch(mode){
    case MODE_INPUT:
      pinMode(pin_map[pin], INPUT);
      break;
    case MODE_OUTPUT:
      pinMode(pin_map[pin], OUTPUT);
      break;
    case MODE_INPUT_PULLUP:
      pinMode(pin_map[pin], INPUT_PULLUP);
      break;
    case MODE_ANALOG:
      pinMode(pin_map[pin], INPUT);
      break;
    case MODE_PWM:
      pinMode(pin_map[pin], OUTPUT);
      break;
  }
  pin_modes[pin] = mode;
}


void receiveEvent(int bytes){
  // handle writes
	if(bytes!=2) return;
	const uint8_t cmd = Wire.read();
  const uint8_t arg = Wire.read();

  const int pin = (cmd>>0x04)-1;
  if(pin<-1 || pin>8) return;

  if(cmd==0x01){
    // heartbeat, ignore
  }
  else if((cmd&0b1111)==0){
    // set pin
    if(pin_modes[pin]==MODE_PWM){
      analogWrite(pin_map[pin], arg);
    }
    else{
      digitalWrite(pin_map[pin], arg);
    }
  }
  else if((cmd&0b1111)==1){
    // set pin mode
    if(arg>=MODE_END) return;
    setPinMode(pin_map[pin], arg);
  }
  else{
    // invalid request
    return;
  }

  last_activity = millis();

	// read data with Wire.read()
	Serial.print("received cmd");
  Serial.print(cmd, HEX);
	Serial.print(" arg ");
  Serial.print(arg, HEX);
	Serial.println();
}

void requestEvent(){
  // handle reads
  const uint8_t cmd = Wire.read();
  const int pin = (cmd>>0x04)-1;
  if(pin<-1 || pin>8) return;

  if(cmd==0x00){
    const uint8_t d[] = {0x68, 0x67};
    Wire.write(d, 2);
  }
  else if(cmd==0x02){
    const uint16_t b = betterAnalogRead(battery_pin)*2;  // there is voltage divider on battery_pin
    Wire.write((uint8_t*)&b, 2);
  }
  else if(cmd==0x03){
    Wire.write(digitalRead(button_pin));
  }
  else if((cmd&0b1111)==0){
    // return state
    Wire.write(digitalRead(pin_map[pin]));
  }
  else if((cmd&0b1111)==1){
    // return mode
    Wire.write(pin_modes[pin]);
  }
  else if((cmd&0b1111)==2){
    if(pin_modes[pin]==MODE_ANALOG){
      const uint16_t v = betterAnalogRead(pin_map[pin]);
      Wire.write((uint8_t*)&v, 2);
    }
  }
  last_activity = millis();

	Serial.print("got req, returning 0x33, arg ");
  Serial.println(Wire.read());
	Wire.write(0x33);
}

void setup(){
  // set power enable pin
  pinMode(power_pin, OUTPUT);
  digitalWrite(power_pin, HIGH);

  // setup pins
  for(int i=0; i<8; i++){
    setPinMode(i, pin_modes[i]);
  }

	Serial.begin(115200);
  Serial.println("hello");
	Wire.begin(I2C_ADDRESS);
	Wire.onRequest(requestEvent);
	Wire.onReceive(receiveEvent);
}

uint32_t button_pressed_tm = 0;
bool button_down = false;
void loop(){
  // check power button
  const bool b = digitalRead(button_pin);
  if(b && !button_down){
    button_pressed_tm = millis();
  }
  button_down = b;
  if(button_down && millis()-button_pressed_tm>button_poweroff_timeout){
    // power off
    Serial.println("power off");
    digitalWrite(power_pin, LOW);
    pinMode(power_pin, OUTPUT);
    digitalWrite(power_pin, LOW);
    while(1); // halt
  }
  delay(100);
}
