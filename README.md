# hgTerm

Small Raspberry Pi powered handheld computing device.

Detailed info available at https://hyperglitch.com/articles/hgterm

Files:

- hgterm.fcstd - FreeCad 3D model of the case
- print/  - printable models
- tools/arduino  - power management and GPIO handling
- tools/brightness.py  - set display brightness
- tools/lxpanel-battery  - LXpanel battery widget



